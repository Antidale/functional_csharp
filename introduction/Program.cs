﻿using System;
using System.Collections.Generic;
using System.Linq;
using static System.Linq.Enumerable;
using static System.Console;
using System.Threading.Tasks;

namespace introduction
{
    class Program
    {
        static void Main(string[] args)
        {
            //FirstClassFunctions();
            //StatelessStortingAndFiltering();
            //ListSortMutates();
            //FurtherMutationExample();
            AvoidingTheIssue();
            ReadKey();
        }

        static void FirstClassFunctions()
        {
            WriteLine("Create a function and pass it around");
            Func<int, int> triple = x => x * 3;
            var range = Range(1, 3);
            var triples = range.Select(triple);
            foreach (var item in triples)
            {
                WriteLine(item);
            }

        }

        static void StatelessStortingAndFiltering()
        {
            WriteLine("Creating an array, then calling sort and filter functions on it that won't change the original");
            Func<int, bool> isOdd = x => x % 2 == 1;
            int[] original = { 7, 6, 1 };
            var sorted = original.OrderBy(x => x);
            var filtered = original.Where(isOdd);
            WriteResults(original, nameof(original));
            WriteResults(filtered, nameof(filtered));
            WriteResults(sorted, nameof(sorted));
        }

        static void ListSortMutates()
        {
            WriteLine("List<T>.Sort() is destructive in that it changes the origional");
            var original = new List<int> { 5, 7, 1 };
            WriteResults(original, nameof(original));
            original.Sort();
            WriteResults(original, nameof(original));
        }

        static void FurtherMutationExample()
        {
            var nums = Range(-10000, 20001).Reverse().ToList();

            Action straightSum = () => WriteLine(nums.Sum());
            Action sortThenSum = () => { nums.Sort(); WriteLine(nums.Sum()); };

            Parallel.Invoke(straightSum, sortThenSum);
            //This didn't actually work on my Air. is the underlying issue fixed in .net Core 2.0? or is there something up with my machine?
        }

        static void AvoidingTheIssue()
        {
            var nums = Range(-10000, 20001).Reverse().ToList();

            Action straightSum = () => WriteLine(nums.Sum());
            Action sortThenSum = () => { nums.Sort(); WriteLine(nums.Sum()); };
            Action safeSort = () => WriteLine(nums.OrderBy(x => x).Sum());

            Parallel.Invoke(straightSum, sortThenSum);
            Parallel.Invoke(straightSum, safeSort);
        }

        static void WriteResults<T>(IEnumerable<T> collection, string collectionName = "")
        {
            WriteLine(collectionName);
            foreach (var item in collection)
            {
                WriteLine(item);
            }
        }
    }
}
